﻿#####Définitions des variables
##liens de téléchargement
## Définir la version de Git à télécharger
$GITdownloadLink = "https://github.com/git-for-windows/git/releases/download/v2.40.0.windows.1/Git-2.40.0-64-bit.exe"
# Définir le lien de téléchargement de Chainsaw
$chainsawDownloadLink = "https://github.com/WithSecureLabs/chainsaw/releases/download/v2.6.0/chainsaw_x86_64-pc-windows-msvc.zip"
# Definir le lien de téléchargement wazuh
$msiDownloadUrl = "https://packages.wazuh.com/4.x/windows/wazuh-agent-4.4.1-1.msi"
# definir le telechargement de yara
#yara
$yaraVersion = "4.3.0"
$yaraUrl = "https://github.com/VirusTotal/yara/releases/download/v4.3.0/yara-4.3.0-2120-win64.zip"
$yaraPath = "C:\altinea"
$yaraExe = "$yaraPath\yara\yara64.exe"
#chainsaw
$chainsawExePath = "C:\altinea\chainsaw\chainsaw.exe"
$chainsawExtractPath = "C:\altinea\"
$chainsawBinPath = "C:\altinea\chainsaw"
#systeme
$dateTime = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
$altineaFolderPath = "C:\Altinea"
# Paramètres Wazuh
$wazuhpass = Read-Host "Veuillez entrer votre mot de passe d'enrôlement Wazuh" -AsSecureString
$wazuhManager = "wazuh.altinea.fr"
$wazuhRegistrationServer = "wazuh.altinea.fr"
$wazuhAgentGroup = "Clients"
$wazuhfolderpath = "C:\Program Files (x86)\ossec-agent" 
# Nom de l'agent Wazuh
$agentName = "altinea_$env:COMPUTERNAME"
$domain = $null

#Parametres git
$gitBinPath = "${env:ProgramFiles}\Git\cmd"

# Fonction pour écrire dans le fichier de log
function Write-Log([string]$message) {
    $logFilePath = "C:\altinea\install.log"
    $logMessage = "$dateTime : $message"
    Add-Content $logFilePath $logMessage
}

# Vérifier si le dossier Altinea existe, sinon le créer
if (!(Test-Path $altineaFolderPath)) {
    New-Item -ItemType Directory -Path $altineaFolderPath | Out-Null
}

#verifie si le poste est dans un domaine pour renomer l'agent Wazuh:
try {
    $domain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
}
catch {
    Write-Host "Le poste n'est pas dans un domaine."
}

if ($domain -ne $null) {
    $domainName = $domain.Name
    $agentName = $domainName + "_" + $env:COMPUTERNAME
}

$agentName = $agentName.ToLower()

# Vérification si l'agent Wazuh doit être installé
if (!(Test-path $wazuhfolderpath)) {
    # Installation de l'agent Wazuh
    Invoke-WebRequest -Uri $msiDownloadUrl -OutFile "$env:temp\wazuh-agent.msi"
    Start-Process -FilePath msiexec.exe -ArgumentList "/i $env:temp\wazuh-agent.msi /q WAZUH_MANAGER=$wazuhManager WAZUH_REGISTRATION_SERVER=$wazuhRegistrationServer WAZUH_REGISTRATION_PASSWORD=$wazuhpass WAZUH_AGENT_GROUP=$wazuhAgentGroup WAZUH_AGENT_NAME=$agentName"
    Start-Sleep -Seconds 5
    Start-Service -Name WazuhSvc
    Start-Sleep -Seconds 5
    Write-Log "L'agent Wazuh a été installé" -ForegroundColor Green
    }
# se deplace dans le repertoire wazuh
cd $wazuhfolderpath
if (!(Get-Command ".\wazuh-agent.exe" -ErrorAction SilentlyContinue)) {
    # ReInstallation de l'agent Wazuh
    Invoke-WebRequest -Uri $msiDownloadUrl -OutFile "$env:temp\wazuh-agent.msi"
    Start-Process -FilePath msiexec.exe -ArgumentList "/i $env:temp\wazuh-agent.msi /q WAZUH_MANAGER=$wazuhManager WAZUH_REGISTRATION_SERVER=$wazuhRegistrationServer WAZUH_REGISTRATION_PASSWORD=$wazuhpass WAZUH_AGENT_GROUP=$wazuhAgentGroup WAZUH_AGENT_NAME=$agentName"
    Start-Sleep -Seconds 5
    Start-Service -Name WazuhSvc
    Start-Sleep -Seconds 5
    Write-Log "L'agent Wazuh a été REINSTALLE" -ForegroundColor Yellow
    
} 
else {
    Write-Log "l'agent Wazuh est bien présent" -ForegroundColor Green
    }
    
# fin installation Wazuh

#installation de Git
# Vérifier si Git est déjà installé
$gitVersion = $null
try {
    $gitVersionString = (git --version).Split()[2]
    $gitVersionRegex = [regex]::match($gitVersionString, '(?<version>\d+(\.\d+)+)')
    if ($gitVersionRegex.Success) {
        $gitVersion = [Version]$gitVersionRegex.Groups['version'].Value
    }
} catch {}

# Trouver la version de Git disponible en téléchargement
$downloadVersionString = [System.IO.Path]::GetFileNameWithoutExtension($GITdownloadLink) -replace 'Git-', '' -replace '-64-bit', ''
$downloadVersion = [Version]$downloadVersionString

# Comparer la version installée avec la version disponible en téléchargement
if (!$gitVersion -or $gitVersion -lt $downloadVersion) {

    # Télécharger la dernière version de Git pour Windows
    $gitInstaller = "git-latest-windows.exe"
    Invoke-WebRequest -Uri $GITdownloadLink -OutFile $gitInstaller

    # Installer Git
    Start-Process -FilePath $gitInstaller -ArgumentList "/VERYSILENT", "/NORESTART", "/LOG=git_install.log" -NoNewWindow -Wait

    # Supprimer le fichier d'installation
    Remove-Item -Path $gitInstaller

    function Add-GitToPath {
    # Vérifier si Git est déjà dans le PATH
     if (-not ($env:Path | Select-String -SimpleMatch $gitBinPath)) {
    # Ajouter Git au PATH système
    Write-Log "Git est ajouté au path"
    $env:Path += ";$gitBinPath"
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";$gitBinPath"
    [Environment]::SetEnvironmentVariable("Path", $env:Path, "Machine")
}
}
}

# Afficher les versions installée et disponible
Write-Log "GIT: Version installée : $($gitVersion.ToString())"
Write-Log "Git: Version disponible en téléchargement : $($downloadVersion.ToString())"

# Fin installation de Git

# Installation de Chainsaw
# Vérifier si Chainsaw est déjà installé
$chainsawVersion = $null

if (Test-Path $chainsawExePath) {
    $chainsawVersionString = (chainsaw --version).Split()[1]
    $chainsawVersion = [Version]$chainsawVersionString
}

# Trouver la version de Chainsaw disponible en téléchargement
$chainsawVersionString = $chainsawDownloadLink -replace '^.*\/download\/v?|\/chainsaw_x86_64.*$',''
$chainsawDownloadVersion = [Version]$chainsawVersionString

# Comparer la version installée avec la version disponible en téléchargement
if (!$chainsawVersion -or $chainsawVersion -lt $chainsawDownloadVersion) {

    # Télécharger Chainsaw
    $chainsawZipFile = "chainsaw.zip"
    Invoke-WebRequest -Uri $chainsawDownloadLink -OutFile $chainsawZipFile

    # Extraire les fichiers de l'archive
    
    Expand-Archive -LiteralPath $chainsawZipFile -DestinationPath $chainsawExtractPath -Force

    # Supprimer l'archive
    Remove-Item -Path $chainsawZipFile

    # Mettre à jour le PATH système (uniquement en cas d'installation)
    function Add-ChainsawToPath {
        if (-not ($env:Path | Select-String -SimpleMatch $chainsawBinPath)) {
            $env:Path += ";$chainsawBinPath"
            [Environment]::SetEnvironmentVariable("Path", $env:Path, "Machine")
        }
    }

    Write-Log "Chainsaw n'est pas installé. Installation en cours..."
    Add-ChainsawToPath
} elseif ($chainsawVersion -ge $chainsawDownloadVersion) {
    Write-Log "Chainsaw est présent dans sa dernière version. Rien à faire."
} else {
    Write-Log "Chainsaw n'est pas à jour. Mise à jour en cours..."
}

########Installation de  Yara
# Vérification de la version de Yara
if (Test-Path $yaraExe) {
    $installedYaraVersion = (yara64.exe --version)
    if ($installedYaraVersion -ge $yaraVersion) {
        Write-Log "La version $yaraVersion de Yara est deja installé dans $yaraPath"
        return
    }
    else {
        Write-Log "La version installé de Yara est: $installedYaraVersion. MAJ en version $yaraVersion"
    }
}
else {
    Write-Log "Yara n'est pas installé. Downloading and installing version $yaraVersion"
}

# Téléchargement de Yara
$yaraZipPath = "$env:TEMP\yara.zip"
Invoke-WebRequest $yaraUrl -OutFile $yaraZipPath
Write-Log "telechargement de yara"

# Décompression de Yara
$yaraTempPath = Join-Path $yaraPath "yara"
Expand-Archive -Path $yaraZipPath -DestinationPath $yaraTempPath -Force

# Renommage du dossier extrait et déplacement dans le dossier yara
$yaraExtractPath = Join-Path $yaraTempPath "yara-$yaraVersion"
$yaraFinalPath = Join-Path $yaraPath "yara"

# Ajout de Yara au PATH
if (-not (Test-Path "Env:\Path")) {
    New-Item -Path "Env:\" -Name "Path" -Value "$yaraPath\yara"
}
elseif (([Environment]::GetEnvironmentVariable("Path", "User") -split ";") -notcontains "$yaraPath\yara") {
    $newPath = "$($env:Path);$yaraPath\yara"
    [Environment]::SetEnvironmentVariable("Path", $newPath, "User")
}

Write-Log "Yara version $yaraVersion installed in $yaraPath and added to PATH"

######## Fin installation Yara

