﻿$gitLabRepoUrl = "https://gitlab.altinea.fr/cyril.gorse/EDR"
$filePath = "edrV2.ps1"
$downloadUrl = "$gitLabRepoUrl/raw/master/$filePath"
$downloadFolder = "$altineaFolderPath\install"
$savePath = Join-Path $downloadFolder $filePath
$altineaFolderPath = "C:\altinea"

# Vérifier si le dossier Altinea existe, sinon le créer
if (!(Test-Path $altineaFolderPath)) {
    New-Item -ItemType Directory -Path $altineaFolderPath | Out-Null
    # Définir le dossier Altinea comme un dossier caché
    (Get-Item $altineaFolderPath).Attributes = 'Hidden'
}
if (!(Test-Path $downloadFolder)) {
    New-Item -ItemType Directory -Path $downloadFolder | Out-Null
    }
Invoke-WebRequest -Uri $downloadUrl -OutFile $savePath

Write-Host "Le fichier a été téléchargé avec succès dans le dossier Altinea."

# Exécuter le script téléchargé
Write-Host "Exécution du script téléchargé..."
& $savePath

$taskName = "AltiCSInstall"
$scriptPath = "C:\altinea\install\$filepath"

# Vérifier si la tâche planifiée existe déjà
$existingTask = Get-ScheduledTask -TaskName $taskName -ErrorAction SilentlyContinue

if ($existingTask -eq $null) {
    # Créer un déclencheur pour le démarrage de l'ordinateur
    $trigger = New-ScheduledTaskTrigger -AtStartup

    # Créer une action pour exécuter le script PowerShell
    $action = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument "-File `"$scriptPath`""

    # Créer une tâche planifiée
    Register-ScheduledTask -TaskName $taskName -Trigger $trigger -Action $action -User "SYSTEM"

    Write-Host "La tâche planifiée '$taskName' a été créée avec succès."
} else {
    Write-Host "La tâche planifiée '$taskName' existe déjà."
}
